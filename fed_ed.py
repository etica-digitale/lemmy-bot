import requests
import xml.etree.ElementTree as ET
from sys import argv
from telegram.ext import CommandHandler, Updater, Application, ContextTypes, MessageHandler, filters
from telegram import MessageEntity, Message, Update
from telegram import constants
from telegram import InlineKeyboardButton, InlineKeyboardMarkup


from datetime import datetime

from html.parser import HTMLParser

_url = "https://feddit.it/feeds/c/eticadigitale.xml?sort=New"
_token = ""
_chat_id = ""


def get_reply_markup(_url, _disc): # if an url exits, creates a Button with the specified url
    reply_markup = None

    if (_url != ""):
        menu = []
        button_list = []

        button_list.append(InlineKeyboardButton("↗️ Leggi l'articolo", callback_data = "Leggi l'articolo", url = _url))
        button_list.append(InlineKeyboardButton("Partecipa alla discussione ↖️", callback_data = "Partecipa alla discussione ↖️", url = _disc))
        menu.append(button_list)

        reply_markup=InlineKeyboardMarkup(menu)


    return reply_markup

#####

# Algorithm that finds the number of sticky posts
# Assuming communities could add or remove sticked posts over time, this must be runned periodically
def find_stickies():
    offset=1

    tree = ET.parse('eticadigitale.xml')

    # Get and parse every dates from channel posts
    dates_list = []
    elems = tree.findall('./channel/item/pubDate')
    for elem in elems:
        to_parse = elem.text[0:len(elem)-6]
        dates_list.append(datetime.strptime(to_parse, '%a, %d %b %Y %H:%M:%S'))
    
    # This only works if you sort xml by New
    # If you find that the next post is older than the current one, then that's a sticky
    for i in range(0,len(dates_list)-1):
        if dates_list[i] < dates_list[i+1]:
            return offset + i + 1

    return offset

# Some characters are reserverd for telegram markdown, so they must be escaped with the precedenting substring "\\"
# https://core.telegram.org/bots/api#markdownv2-style
def clear_markdown_parse(msg): 
    msg = msg.replace("&#x27;","'")

    reserved = ['_', '*', '[', ']', '(', ')', '~', '`', '>', '#', '+', '-', '=', '|', '{', '}', '.', '!']

    print(len(msg))

    #fix last char
#    for ch in reserved:
#        if msg[len(msg)-1] == ch and msg[len(msg)-2] != '\\':
#            msg = msg[:len(msg)-1] + "\\" + msg[len(msg)-1]

    flag=False

    for i in range(0,len(msg)):
        for ch in reserved:

            if msg[i] == ch and msg[i-1] != '\\':
                msg = msg[:i] + "\\" + msg[i:]
                if msg[len(msg)-1] == ch:
                    flag=True
    if flag==True:
        msg = msg[:len(msg)-1] + "\\" + msg[len(msg)-1]

    
    print("lunghezza adesso: " + str(len(msg)))
    print(msg)
    return msg

# Reads the old link from file, if it doesn't matches the new one, then it's a new post
async def anythingNew(link):
    old_link = ""
    try:
        with open('.lastone','r') as f: # Read last message sent
            old_link = f.read()
    except FileNotFoundError: # Probably first run of the program
            pass
    
    if (link != old_link): # check if new msg is different from last one
        with open('.lastone','w') as f:
            f.write(link)
        return True
    
    return False
    

# Take the third child of the XML tree
# Returns a message to send, the link to the feddit discussion and (if exists) the url
def parseXML():
    global _url
    resp = requests.get(_url)
    link=""

    with open('eticadigitale.xml','wb') as f:
        f.write(resp.content)
    
    tree = ET.parse("eticadigitale.xml")

    url = ""
    msg = ""
    linkToIns = ""
    linkToDiscussion= ""
    offset = find_stickies()
    elem = tree.findall("./channel/item[" + str(offset) + "]/")
    for subelem in elem:
            if (subelem.tag == "guid"):
                linkToDiscussion=subelem.text
            elif (subelem.tag == "title"):
                print(subelem.text)
                msg = msg + "*" + clear_markdown_parse(subelem.text) +  "* \n"    
            elif (subelem.tag=="link"):
                linkToIns = subelem.text
    

    msg = "📬 _Nuova discussione_\n" + f"[{msg}]({linkToIns})" + "\n_Vieni a trovarci su [Lemmy](https://t\\.me/eticadigitale/648)_"

    return msg, linkToIns,linkToDiscussion

async def work(context):
    global _chat_id

    if _chat_id == "":
        return

    msg, linkToIns,linkToDiscussion = parseXML()
    reply_markup = get_reply_markup(linkToIns, linkToDiscussion)

    if await anythingNew(linkToIns) == True:
        if reply_markup != None:
            await context.bot.send_message(chat_id=_chat_id, text=msg, reply_markup=reply_markup, parse_mode=constants.ParseMode.MARKDOWN_V2, disable_web_page_preview=True )
        else:
            await context.bot.send_message(chat_id=_chat_id, text=msg, parse_mode=constants.ParseMode.MARKDOWN_V2, disable_web_page_preview=True )


async def start(update, context): # Not needed, maybe will be deleted in the next release
    global _chat_id

    print(update.message.chat_id)

    if (_chat_id == ""):
        with open(".config") as f:
            _chat_id = f.read()

    if _chat_id != "":
        await context.bot.send_message(chat_id = _chat_id, text = "Bot inizializzato\\." ,parse_mode=constants.ParseMode.MARKDOWN_V2)

        

async def pong(update: Update, context: ContextTypes.DEFAULT_TYPE):
    global _chat_id

    #if _chat_id != "":
    #    context.bot.send_message(chat_id = _chat_id, text = "Pong")
    await update.message.reply_text("Pong")


def main():
    global _token
    global _chat_id

    
    if ( len(argv) > 1):
        _token = argv[1]

    if _token == "":
        print("Token is empty")
        return

    
    if (_chat_id == ""):
        with open(".config") as f:
            _chat_id = f.read()
    
    
    application = Application.builder().token(_token).build()

    application.add_handler(CommandHandler('start',start))
    application.add_handler(CommandHandler('ping', pong))

    j = application.job_queue

    #j.run_repeating(work, interval=800, first=5)
    j.run_repeating(work, interval=10,first=5)

    application.run_polling(allowed_updates=Update.ALL_TYPES)

                

if __name__=="__main__":
    main()
